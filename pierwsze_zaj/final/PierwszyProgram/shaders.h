#ifndef SHADERS_H
#define SHADERS_H

	#include <GL\glew.h>
	#include <GL\freeglut.h>
	#include <iostream>
	#include <vector>

#endif /*SHADER_H*/

#pragma once




class Shaders
{
public:
	Shaders(void);
	~Shaders(void);

	Shaders(int screenWidth,int screenHeigth, int posX, int posY);

	void initGlew();
	void createVAO();
	void createVBO();
	void createProgram();
	void addToProgram(GLuint programZShaderami, const char* tekstShadera, GLenum typShadera);

	void CreateWindowGlut(int argc, char** argv);

	static void Display();


private:
	int screenWidth;
	int screenHeight;
	int posX;
	int posY;
	GLuint programZShaderami;
};

