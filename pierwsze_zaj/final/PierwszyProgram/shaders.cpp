#include "shaders.h"


GLuint VAO;
GLuint VBO;

Shaders::Shaders()
{
	this->screenWidth = 1024;
	this->screenHeight = 768;
	this->posX = 100;
	this->posY = 100;
}

Shaders::Shaders(int screenWidth, int screenHeigth, int posX, int posY)
{
	this->screenWidth = screenWidth;
	this->screenHeight = screenHeigth;
	this->posX = posX;
	this->posY = posY;
}

Shaders::~Shaders()
{

}


void Shaders::CreateWindowGlut(int argc, char ** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(screenWidth, screenHeight);
	glutInitWindowPosition(posX, posY);
	glutCreateWindow("punkt");
}

void Shaders::Display()
{
	glClear(GL_COLOR_BUFFER_BIT);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glDrawArrays(GL_TRIANGLES, 0, 3);

	glDisableVertexAttribArray(0);

	glutSwapBuffers();
}

void Shaders::initGlew()
{
	GLenum wynik = glewInit();
	if (wynik != GLEW_OK){
		std::cerr << "Nie udalo sie zainicjalizowac GLEW. Blad:" <<
							glewGetErrorString(wynik) << std::endl;
		system("pause");
		exit(1);
	}
}

void Shaders::createVAO()
{
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);
}

void Shaders::createVBO()
{
	GLfloat Wierzcholki[9] = { -1.0f, -1.0f, 0.0f,
								1.0f, -1.0f, 0.0f,
								0.0f,  1.0f, 0.0f };

	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Wierzcholki), Wierzcholki, GL_STATIC_DRAW);
}

void Shaders::createProgram()
{
	programZShaderami = glCreateProgram();

	if (programZShaderami == 0)
	{
		std::cerr << "Blad podczas tworzenia programu shaderow." << std::endl;
		system("pause");
		exit(1);
	}

	char * vertexShader =
		"#version 330\n"
		"layout (location = 0) in vec3 polozenie;"
		"void main()"
		"{"
		"gl_Position = vec4(0.5 * polozenie.x, 0.5 * polozenie.y, polozenie.z, 1.0);"
		"}";

	char * fragmentShader =
	"#version 330\n"
		"out vec4 kolor;"
		"void main()"
		"{"
		"kolor = vec4(0.8, 0.8, 0.0, 1.0);"
		"}";

	addToProgram(programZShaderami, vertexShader, GL_VERTEX_SHADER);
	addToProgram(programZShaderami, fragmentShader, GL_FRAGMENT_SHADER);

	GLint linkowanieOK = 0;
	glLinkProgram(programZShaderami);
	glGetProgramiv(programZShaderami, GL_LINK_STATUS, &linkowanieOK);
	if (linkowanieOK == GL_FALSE){
		GLint dlugoscLoga = 0;
		glGetShaderiv(programZShaderami, GL_INFO_LOG_LENGTH, &dlugoscLoga);
		std::vector<GLchar> log(dlugoscLoga);

		glGetProgramInfoLog(programZShaderami, dlugoscLoga, NULL, &log[0]);
		std::cerr << "Blad podczas linkowania programu shader�w." << std::endl;
		for (std::vector<GLchar>::const_iterator i = log.begin(); i != log.end(); ++i) std::cerr << *i;
		std::cerr << std::endl;
		glDeleteProgram(programZShaderami);
		system("pause");
		exit(1);
	}

	GLint walidacjaOK = 0;
	glValidateProgram(programZShaderami);
	glGetProgramiv(programZShaderami, GL_LINK_STATUS, &linkowanieOK);
	if (linkowanieOK == GL_FALSE){
		GLint dlugoscLoga = 0;
		glGetProgramiv(programZShaderami, GL_INFO_LOG_LENGTH, &dlugoscLoga);
		std::vector<GLchar> log(dlugoscLoga);
		glGetProgramInfoLog(programZShaderami, dlugoscLoga, NULL, &log[0]);
		std::cerr << "Blad podczas walidacji programu shader�w." << std::endl;
		for (std::vector<GLchar>::const_iterator i = log.begin(); i != log.end(); ++i) std::cerr << *i;
		std::cerr << std::endl;
		glDeleteProgram(programZShaderami);
		system("pause");
		exit(1);
	}
	glUseProgram(programZShaderami);

}

void Shaders::addToProgram(GLuint programZShaderami, const GLchar * tekstShadera, GLenum typShadera)
{
	GLuint shader = glCreateShader(typShadera);

	if (shader == 0){
		std::cerr << "Blad podczas tworzenia programu shadera" << std::endl;
		system("pause");
		exit(1);
	}

	const GLchar * tekstShaderaTab[1];
	tekstShaderaTab[0] = tekstShadera;
	GLint dlugoscShadera[1];
	dlugoscShadera[0] = strlen(tekstShadera);
	glShaderSource(shader, 1, tekstShaderaTab, dlugoscShadera);
	glCompileShader(shader);
	GLint kompilacjaOK = 0;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &kompilacjaOK);
	if (kompilacjaOK == GL_FALSE){
		GLint dlugoscLoga = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &dlugoscLoga);
		std::vector<GLchar> log(dlugoscLoga);
		glGetShaderInfoLog(shader, dlugoscLoga, NULL, &log[0]);
		std::cerr << "Blad podczas kompilacji shadera." << std::endl;
		for (std::vector<GLchar>::const_iterator i = log.begin(); i != log.end(); ++i) std::cerr << *i;
		std::cerr << std::endl;
		system("pause");
		exit(1);
	}

	glAttachShader(programZShaderami, shader);

}

int main(int argc, char ** argv)
{
Shaders shaders(786, 1024, 100, 100);

shaders.CreateWindowGlut(argc, argv);
shaders.initGlew();
shaders.createVAO();
shaders.createVBO();

shaders.createProgram();

glutDisplayFunc(shaders.Display);
// po zamknieciu okna kontrola wraca do programu
glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);

glClearColor(0.2f, 0.1f, 0.0f, 0.0f);

glutMainLoop();

return 0;
}